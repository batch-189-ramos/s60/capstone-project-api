const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	
	userPurchaseDetails: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required"]
			}
			
		}
	]
})


module.exports = mongoose.model("Product", productSchema);


/*quantity: {
	type: Number,
	required: [true, "Quantity of order per Item is required"]
},
subTotal: {
	type: Number,
	required: [true, "Sub Total Amount is required"]
},
orderStatus:{
	type: String,
	default: "Order on Process"
}*/