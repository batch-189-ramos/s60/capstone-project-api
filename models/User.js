const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},
	orders : [
		{
			productId : {
				type : String,
				required : [true, "Product ID is required"]
			}
			
		}
	]
})

module.exports = mongoose.model("User", userSchema);

/*productName: {
	type: String,
	required: [true, "Product Name is required"]
},
productPrice: {
	type: Number,
	required: [true, "Product Price is required"]
},
quantity: {
	type: Number,
	required: [true, "No. of items per product required"]
},
subTotal: {
	type: Number,
	required: [true, "Total Amount must be computed"]
},
purchasedOn : {
	type : Date,
	default : new Date()
}
*/