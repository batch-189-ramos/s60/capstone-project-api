const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


//Route for creating a product
// router.post("/", (req, res) => {
// 	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
// });

//Session 39 - Activity - Solution 1
/*router.post('/createProducts', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send({auth: "failed"})
	}
});
*/

router.post("/checkProduct", (req, res) => {
	productController.checkProductExists(req.body).then(resultFromController =>res.send(resultFromController))
});

//Session 39 - Activity Solution 2
router.post("/createProducts", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

//Route for retrieving all the products
router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});

//Route for retrieving all active products
// Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the products
router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

//Route for retrieving a specific product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:productId", (req, res) => {

	console.log(req.params)

	// Since the product ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the product ID by accessing the request's "params" property which contains all the parameters provided via the url
		// Example: URL - http://localhost:4000/products/613e926a82198824c8c4ce0e
		// The product Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "productId" in the route
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))

});


//Route for updating a product
// Route for updating a product
// JWT verification is needed for this route to ensure that a user is logged in before updating a product
router.put("/:productId", auth.verify, (req, res) => {

	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))

})

// Session 40 - Activity - Solution
// Route to archiving a product
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the products from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put('/:productId/archive', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		payload : auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});

// router.put('/:productId/archive', auth.verify, (req, res) => {

// 	const data = {
// 		productId : req.params.productId,
// 		payload : auth.decode(req.headers.authorization).isAdmin
// 	}

// 	productController.archiveProduct(data, req.body).then(resultFromController => res.send(resultFromController))
// });




module.exports = router;