const Product = require('../models/Product');
const User = require('../models/User');

//Create a new Product
module.exports.addProduct = (reqBody) => {

	// Creates a variable "newProduct" and instantiates a new "Product" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	});


	// Saves the created object to our database
	return newProduct.save().then((product, error) => {

		// Product creation failed
		if(error) {

			return false

		// Product creation successful
		} else {

			return true
		}
	})
}

module.exports.checkProductExists = (reqBody) => {
	return Product.find({name: reqBody.name}).then(result => {

		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
}

//Activity Solution 2
module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return false
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price,
                stocks: reqBody.stocks
            })
        
            //Saves the created object to the database
            return newProduct.save().then((Product, error) => {
                //Product creation failed
                if(error) {
                    return false
                } else {
                    //Product creation successful
                    return true
                }
            })
        }
        
    });    
}


//Retrieve All Products
module.exports.getAllProducts = async (data) => {

	if (data.isAdmin) {
		return Product.find({}).then(result => {

			return result
		})
	} else {

		return false
	}


}

//Retrieve All Active Products
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then(result => {

		return result
	})

}

//Retrieve A Specific Product
module.exports.getProduct = (reqParams) => {


	return Product.findById(reqParams.productId).then(result => {

		return result
	})

}


//Update A Specific Product
// Information to update a product will be coming from both the URL parameters and the request body
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedProduct = {

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stocks: reqBody.stocks
	}

	// Syntax
			// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) =>{

		// product not updated
		if(error) {

			return false

		// product updated successfully
		} else {

			return true
		
		}


	})


}


// Session 40 - Activity - Solution
// Archive A Product
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the Product "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active Products are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently
module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, err) => {

		if(data.payload === true) {

			result.isActive = false;

			return result.save().then((archivedProduct, err) => {

				// Product not archived
				if(err) {

					return false;

				// Product archived successfully
				} else {


					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}


// module.exports.archiveProduct = (data, reqBody) => {

// 	if (data.payload === true) {

// 		let updateActiveField = {
// 			isActive: reqBody.isActive
// 		}

// 		return Product.findByIdAndUpdate(data.ProductId, updateActiveField).then((Product, err) => {

// 				if(err) {
				
// 					return true
// 				}  else {

// 					return false
// 				}
// 		})
		 
// 	} else {

// 		return false
// 	} 

// }