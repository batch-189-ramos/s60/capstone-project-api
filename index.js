const express = require('express');
const mongoose = require('mongoose');
//Allows our backend application to be available to our frontend application
//Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

const port = 4000;
//Creates an "app" variable that stores the result of the "express" function that initializes our express application and allows us access to different methods that will make backend creation easy
const app = express();

//Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);
app.use("/products", productRoutes);

//Connect to our MongoDB database
mongoose.connect("mongodb+srv://RuRuSensei:admin123@zuitt-bootcamp.5jpfbgk.mongodb.net/ordering-system-API?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));

//Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
//This syntax will allow flexibility when using the application locally or as a hosted application		
app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})